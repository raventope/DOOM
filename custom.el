(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("c:/Users/glenn/Dropbox/org/org-gtd/org-gtd-tasks.org" "c:/Users/glenn/Dropbox/org/Habits.org" "c:/Users/glenn/Dropbox/org/Tasks.org" "c:/Users/glenn/Dropbox/org/To-Do.org"))
 '(package-selected-packages '(all-the-icons)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:slant italic))))
 '(font-lock-keyword-face ((t (:slant italic)))))
